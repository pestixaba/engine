<?php
/**
 * Minds Subscriptions
 *
 * @version 1
 * @author Mark Harding
 */
namespace Minds\Controllers\api\v1;

use Firebase\JWT\JWT;
use Minds\Core;
use Minds\Core\Config;
use Minds\Core\Security;
use Minds\Core\Session;
use Minds\Core\Features;
use Minds\Core\Di\Di;
use Minds\Entities;
use Minds\Interfaces;
use Minds\Api\Factory;
use Minds\Exceptions\TwoFactorRequired;
use Minds\Core\Queue;
use Minds\Core\Subscriptions;
use Minds\Core\Analytics;
use Strobotti\JWK\KeyConverter;
use Strobotti\JWK\KeyFactory;
use Strobotti\JWK\KeySet;
use Zend\Diactoros\ServerRequestFactory;

class authenticate implements Interfaces\Api, Interfaces\ApiIgnorePam
{
    /**
     * NOT AVAILABLE
     */
    public function get($pages)
    {
        return Factory::response(['status'=>'error', 'message'=>'GET is not supported for this endpoint']);
    }

    /**
     * Registers a user
     * @param array $pages
     *
     * @SWG\Post(
     *     summary="Create a new channel",
     *     path="/v1/register",
     *     @SWG\Response(name="200", description="Array")
     * )
     */
    public function post($pages)
    {
        return $this->cognito();

        if (!Core\Security\XSRF::validateRequest()) {
            return false;
        }

        $user = new Entities\User(strtolower($_POST['username']));

        /** @var Core\Security\LoginAttempts $attempts */
        $attempts = Core\Di\Di::_()->get('Security\LoginAttempts');

        if (!$user->username) {
            header('HTTP/1.1 401 Unauthorized', true, 401);
            return Factory::response(['status' => 'failed']);
        }

        $attempts->setUser($user);

        if ($attempts->checkFailures()) {
            return Factory::response([
                'status' => 'error',
                'message' => 'LoginException::AttemptsExceeded'
            ]);
        }

        if (!$user->isEnabled() && !$user->isBanned()) {
            $user->enable();
        }

        try {
            if (!Core\Security\Password::check($user, $_POST['password'])) {
                $attempts->logFailure();
                header('HTTP/1.1 401 Unauthorized', true, 401);
                return Factory::response(['status' => 'failed']);
            }
        } catch (Core\Security\Exceptions\PasswordRequiresHashUpgradeException $e) {
            $user->password = Core\Security\Password::generate($user, $_POST['password']);
            $user->override_password = true;
            $user->save();
        }

        $attempts->resetFailuresCount(); // Reset any previous failed login attempts

        try {
            $twoFactorManager = Di::_()->get('Security\TwoFactor\Manager');
            $twoFactorManager->gatekeeper($user, ServerRequestFactory::fromGlobals());
        } catch (\Exception $e) {
            header('HTTP/1.1 ' + $e->getCode(), true, $e->getCode());
            $response['status'] = "error";
            $response['code'] = $e->getCode();
            $response['message'] = $e->getMessage();
            $response['errorId'] = str_replace('\\', '::', get_class($e));
            return Factory::response($response);
        }

        $sessions = Di::_()->get('Sessions\Manager');
        $sessions->setUser($user);
        $sessions->createSession();
        $sessions->save(); //save to db and cookie

        \set_last_login($user); // TODO: Refactor this

        Session::generateJWTCookie($sessions->getSession());
        Security\XSRF::setCookie(true);

        // Set the canary cookie
        Di::_()->get('Features\Canary')
            ->setCookie($user->isCanary());

        // Record login events
        $event = new Analytics\Metrics\Event();
        $event->setUserGuid($user->getGuid())
            ->setType('action')
            ->setAction('login')
            ->push();

        $response['status'] = 'success';
        $response['user'] = $user->export();

        return Factory::response($response);
    }

    /**
     * Authenticates a user with Amazon Cognito
     * @param array $pages
     *
     * @SWG\Post(
     *     summary="Verifies Cognito JWT and logs user in, or registers if doesn't exist",
     *     path="/v1/authenticate",
     *     @SWG\Response(name="200", description="Array")
     * )
     */
    public function cognito()
    {
        if (!Core\Security\XSRF::validateRequest()) {
            return false;
        }

        $token = $_POST['idToken'];

        $jwt = explode('.', $token);

        $header = json_decode(base64_decode($jwt[0]));

        try {
            $region = Config::_()->get('cognito')['region'];
            $userPoolId = Config::_()->get('cognito')['user_pool_id'];
        } catch (\Exception $e) {
            var_dump($e);
        }

        $client = new Core\Http\Curl\Json\Client();
        $keys = $client->get('https://cognito-idp.' . $region . '.amazonaws.com/' . $userPoolId . '/.well-known/jwks.json')['keys'];

        $keyFactory = new KeyFactory();
        $key1 = $keyFactory->createFromJson(json_encode($keys[0]));
        $key2 = $keyFactory->createFromJson(json_encode($keys[1]));

        $keySet = new KeySet();
        $keySet->addKey($key1);
        $keySet->addKey($key2);

        $key = $keySet->getKeyById($header->kid);

        $pem = (new KeyConverter())->keyToPem($key);

        try {
            $userAttributes = JWT::decode($token, $pem, ['RS256']);
        } catch (\Exception $e) {
            header('HTTP/1.1 401 Unauthorized', true, 401);
        }

        $username = $userAttributes->{'cognito:username'};
        $email = $userAttributes->email;

        $user = new Entities\User($username);

        if (!$user->username) {
            $password = generate_strong_password();

            try {
                $user = register_user($username, $password, $username, $email, false);
            } catch (\Exception $e) {
                error_log(
                    "RegistrationError | username: ".$_POST['username']
                    .", email:".$_POST['email']
                    .", signupParentId".$user->signupParentId
                    .", exception: ".$e->getMessage()
                    .", addr: " . $_SERVER['HTTP_X_FORWARDED_FOR']
                );
                $response = ['status' => 'error', 'message' => $e->getMessage()];
            }
        }

        if (!$user->isEnabled() && !$user->isBanned()) {
            $user->enable();
        }

        $sessions = Di::_()->get('Sessions\Manager');
        $sessions->setUser($user);
        $sessions->createSession();
        $sessions->save(); //save to db and cookie

        \set_last_login($user); // TODO: Refactor this

        Session::generateJWTCookie($sessions->getSession());
        Security\XSRF::setCookie(true);

        // Set the canary cookie
        Di::_()->get('Features\Canary')
            ->setCookie($user->isCanary());

        // Record login events
        $event = new Analytics\Metrics\Event();
        $event->setUserGuid($user->getGuid())
            ->setType('action')
            ->setAction('login')
            ->push();

        $response['status'] = 'success';
        $response['user'] = $user->export();

        return Factory::response($response);
    }

    public function put($pages)
    {
    }

    public function delete($pages)
    {
        /** @var Core\Sessions\Manager $sessions */
        $sessions = Di::_()->get('Sessions\Manager');

        if (isset($pages[0]) && $pages[0] === 'all') {
            $sessions->deleteAll();
        } else {
            $sessions->delete();
        }

        return Factory::response([]);
    }
}
